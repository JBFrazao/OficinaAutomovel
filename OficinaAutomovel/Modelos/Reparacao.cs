﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace OficinaAutomovel.Modelos
{
    public partial class Reparacao
    {
        public DataClasses1DataContext dc = new DataClasses1DataContext();

        public int NovoIdReparacao { get; set; }

        public List<Reparacao> CarregarLista()
        {
            List<Reparacao> ListaDeReparacoes = new List<Reparacao>();
            var LR = from Reparacao in dc.Reparacaos select Reparacao;
            int IDR = 0;
            foreach (var R in LR)
            {
                ListaDeReparacoes.Add(R);
                if (R.idReparacao > IDR)
                {
                    IDR = R.idReparacao;
                }
            }
            NovoIdReparacao = IDR + 1;
            return ListaDeReparacoes;
        }


        //Inserir novos dados:
        public void InserirNovaReparacao(Reparacao novaReparacao)
        {
            dc.Reparacaos.InsertOnSubmit(novaReparacao);

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }
        }
     

    }
}
