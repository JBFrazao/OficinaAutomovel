﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace OficinaAutomovel.Modelos
{
    public partial class Cliente
    {
        public DataClasses1DataContext dc = new DataClasses1DataContext();

        public int NovoIdCliente { get; set; }

        public List<Cliente> CarregarLista()
        {
            List<Cliente> ListaDeClientes = new List<Cliente>();
            var LC = from Cliente in dc.Clientes select Cliente;
            int IDC = 0;
            foreach (var C in LC)
            {
                ListaDeClientes.Add(C);
                if (C.idCliente > IDC)
                {
                    IDC = C.idCliente;
                }
            }
            NovoIdCliente = IDC + 1;
            return ListaDeClientes;
        }


        //Inserir novos dados:
        public void InserirNovoCliente(Cliente novoCliente)
        {
            dc.Clientes.InsertOnSubmit(novoCliente);

            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }
        }
     

    }
}
