﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace OficinaAutomovel.Modelos
{
    public partial class Veiculo
    {
        public DataClasses1DataContext dc = new DataClasses1DataContext();
        
        public int NovoIdVeiculo { get; set; }

        public List<Veiculo> CarregarLista()
        {
            List<Veiculo> ListaDeVeiculos = new List<Veiculo>();
            var LV = from Veiculo in dc.Veiculos select Veiculo;
            int IDV = 0;
            foreach (var V in LV)
            {
                ListaDeVeiculos.Add(V);
                if (V.idVeiculo > IDV)
                {
                    IDV = V.idVeiculo;
                }
            }
            NovoIdVeiculo = IDV + 1;
            return ListaDeVeiculos;
        }



        public void InserirNovoVeiculo(Veiculo novoVeiculo)
        {
            dc.Veiculos.InsertOnSubmit(novoVeiculo);
            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }
        }
        //TODOS OS METODOS DE CRUD



    }
}
