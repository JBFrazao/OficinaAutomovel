﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;

namespace OficinaAutomovel.Modelos
{
    public partial class Marcacao
    {
        public DataClasses1DataContext dc = new DataClasses1DataContext();

        public int NovoIdMarcacao { get; set; }

        public List<Marcacao> CarregarLista()
        {
            List<Marcacao> ListaDeMarcacoes = new List<Marcacao>();
            var LM = from Marcacao in dc.Marcacaos select Marcacao;
            int IDM = 0;
            foreach (var M in LM)
            {
                ListaDeMarcacoes.Add(M);
                if (M.idMarcacao > IDM)
                {
                    IDM = M.idMarcacao;
                }
            }
            NovoIdMarcacao = IDM + 1;
            return ListaDeMarcacoes;
        }

        public void InserirNovaMarcacao(Marcacao novaMarcacao)
        {
            dc.Marcacaos.InsertOnSubmit(novaMarcacao);
            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }
        }

        public void Desmarcar(int idMarcacao)
        {
            var pesquisaIdMarcacao = from Marcacao in dc.Marcacaos where Marcacao.idMarcacao == idMarcacao select Marcacao;

            Marcacao Marc = new Marcacao();
            Marc = pesquisaIdMarcacao.Single();
            Marc.Estado = "cancelado";
            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }

        }

        public void Remarcar(int idMarcacao, DateTime dt)
        {
            var pesquisaIdMarcacao = from Marcacao in dc.Marcacaos where Marcacao.idMarcacao == idMarcacao select Marcacao;

            Marcacao Marc = new Marcacao();
            Marc = pesquisaIdMarcacao.Single();
            Marc.Data = dt.Date;
            Marc.Hora = TimeSpan.Parse(dt.ToLongTimeString());
            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);

            }

        }

        public int IniciarReparacao(int idMarcacao)
        {
            var pesquisaIdMarcacao = from Marcacao in dc.Marcacaos where Marcacao.idMarcacao == idMarcacao select Marcacao;

            Marcacao Marc = new Marcacao();
            Marc = pesquisaIdMarcacao.Single();
            Marc.Estado = "aceite";            
            try
            {
                dc.SubmitChanges();
            }
            catch (Exception e)
            {
                System.Windows.Forms.MessageBox.Show(e.Message);
            }

            return idMarcacao;
        }

        public int PesquisarMarcacao(string matricula)
        {
            var pesquisaIdVeiculo = from Veiculo in dc.Veiculos
                           where Veiculo.Matricula == matricula
                           select Veiculo.idVeiculo;
            int idVeiculo = pesquisaIdVeiculo.Single();
            var pesquisaIdMarcacao = from Marcacao in dc.Marcacaos where Marcacao.idVeiculo == idVeiculo && Marcacao.Estado == "em espera..." select Marcacao.idMarcacao;
            int idMarcacao = pesquisaIdMarcacao.Single();
            return idMarcacao;
        }

        public void PesquisarMarcacao(int yaboi)
        {

        }
    }
}
