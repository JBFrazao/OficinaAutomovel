﻿namespace OficinaAutomovel
{
    partial class FormMarcacao
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DGVMarcacao = new System.Windows.Forms.DataGridView();
            this.TabConMarcacoes = new System.Windows.Forms.TabControl();
            this.TPNovaMarcacao = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.BtnCancelar = new System.Windows.Forms.Button();
            this.BtnGuardar = new System.Windows.Forms.Button();
            this.BtnDesmarcar = new System.Windows.Forms.Button();
            this.BtnIniciarReparacao = new System.Windows.Forms.Button();
            this.BtnRemarcar = new System.Windows.Forms.Button();
            this.tabPage9 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.NVModeloCmbBox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.NVMarcaCmbBox = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.NVAnoTxtBox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.PVValidarPicBox = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.NVMatTxtBoxC = new System.Windows.Forms.TextBox();
            this.NVMatTxtBoxA = new System.Windows.Forms.TextBox();
            this.NVMatTxtBoxB = new System.Windows.Forms.TextBox();
            this.tabcontrol = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.PCValidarPicBox = new System.Windows.Forms.PictureBox();
            this.NCNIFTxtBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.NCContactoTxtBox = new System.Windows.Forms.TextBox();
            this.NCNomeTxtBox = new System.Windows.Forms.TextBox();
            this.NCApelidoTxtBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.PMValidarPicBox = new System.Windows.Forms.PictureBox();
            this.SubTimeBtn = new System.Windows.Forms.Button();
            this.SubDaysBtn = new System.Windows.Forms.Button();
            this.AddDaysBtn = new System.Windows.Forms.Button();
            this.AddTimeBtn = new System.Windows.Forms.Button();
            this.NMTxtBoxData = new System.Windows.Forms.TextBox();
            this.NMTxtBoxHora = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.BtnCriarMarcacao = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGVMarcacao)).BeginInit();
            this.TabConMarcacoes.SuspendLayout();
            this.TPNovaMarcacao.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PVValidarPicBox)).BeginInit();
            this.tabcontrol.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PCValidarPicBox)).BeginInit();
            this.tabControl2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PMValidarPicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // DGVMarcacao
            // 
            this.DGVMarcacao.AllowUserToAddRows = false;
            this.DGVMarcacao.AllowUserToDeleteRows = false;
            this.DGVMarcacao.AllowUserToResizeColumns = false;
            this.DGVMarcacao.AllowUserToResizeRows = false;
            this.DGVMarcacao.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.DGVMarcacao.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.DGVMarcacao.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.DGVMarcacao.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.DGVMarcacao.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVMarcacao.ColumnHeadersVisible = false;
            this.DGVMarcacao.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DGVMarcacao.Location = new System.Drawing.Point(610, 0);
            this.DGVMarcacao.MultiSelect = false;
            this.DGVMarcacao.Name = "DGVMarcacao";
            this.DGVMarcacao.ReadOnly = true;
            this.DGVMarcacao.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.DGVMarcacao.RowHeadersVisible = false;
            this.DGVMarcacao.RowTemplate.Height = 50;
            this.DGVMarcacao.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.DGVMarcacao.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.DGVMarcacao.Size = new System.Drawing.Size(390, 430);
            this.DGVMarcacao.TabIndex = 0;
            this.DGVMarcacao.Visible = false;
            // 
            // TabConMarcacoes
            // 
            this.TabConMarcacoes.Controls.Add(this.TPNovaMarcacao);
            this.TabConMarcacoes.Dock = System.Windows.Forms.DockStyle.Left;
            this.TabConMarcacoes.Location = new System.Drawing.Point(0, 0);
            this.TabConMarcacoes.Name = "TabConMarcacoes";
            this.TabConMarcacoes.SelectedIndex = 0;
            this.TabConMarcacoes.Size = new System.Drawing.Size(610, 430);
            this.TabConMarcacoes.TabIndex = 0;
            // 
            // TPNovaMarcacao
            // 
            this.TPNovaMarcacao.Controls.Add(this.tabControl1);
            this.TPNovaMarcacao.Controls.Add(this.tabPage9);
            this.TPNovaMarcacao.Controls.Add(this.tabcontrol);
            this.TPNovaMarcacao.Controls.Add(this.tabControl2);
            this.TPNovaMarcacao.Location = new System.Drawing.Point(4, 22);
            this.TPNovaMarcacao.Name = "TPNovaMarcacao";
            this.TPNovaMarcacao.Padding = new System.Windows.Forms.Padding(3);
            this.TPNovaMarcacao.Size = new System.Drawing.Size(602, 404);
            this.TPNovaMarcacao.TabIndex = 0;
            this.TPNovaMarcacao.Text = "Nova Marcação";
            this.TPNovaMarcacao.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Right;
            this.tabControl1.Location = new System.Drawing.Point(445, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tabControl1.RightToLeftLayout = true;
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(154, 256);
            this.tabControl1.TabIndex = 3;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.BtnCancelar);
            this.tabPage1.Controls.Add(this.BtnGuardar);
            this.tabPage1.Controls.Add(this.BtnDesmarcar);
            this.tabPage1.Controls.Add(this.BtnIniciarReparacao);
            this.tabPage1.Controls.Add(this.BtnRemarcar);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(146, 230);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Opções";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // BtnCancelar
            // 
            this.BtnCancelar.Enabled = false;
            this.BtnCancelar.Location = new System.Drawing.Point(33, 143);
            this.BtnCancelar.Name = "BtnCancelar";
            this.BtnCancelar.Size = new System.Drawing.Size(84, 32);
            this.BtnCancelar.TabIndex = 59;
            this.BtnCancelar.Text = "Cancelar";
            this.BtnCancelar.UseVisualStyleBackColor = true;
            this.BtnCancelar.Click += new System.EventHandler(this.Cancelar_Click);
            // 
            // BtnGuardar
            // 
            this.BtnGuardar.Enabled = false;
            this.BtnGuardar.Location = new System.Drawing.Point(33, 105);
            this.BtnGuardar.Name = "BtnGuardar";
            this.BtnGuardar.Size = new System.Drawing.Size(84, 32);
            this.BtnGuardar.TabIndex = 58;
            this.BtnGuardar.Text = "Guardar";
            this.BtnGuardar.UseVisualStyleBackColor = true;
            this.BtnGuardar.Click += new System.EventHandler(this.BtnGuardar_Click);
            // 
            // BtnDesmarcar
            // 
            this.BtnDesmarcar.Location = new System.Drawing.Point(33, 181);
            this.BtnDesmarcar.Name = "BtnDesmarcar";
            this.BtnDesmarcar.Size = new System.Drawing.Size(84, 32);
            this.BtnDesmarcar.TabIndex = 57;
            this.BtnDesmarcar.Text = "Desmarcar";
            this.BtnDesmarcar.UseVisualStyleBackColor = true;
            this.BtnDesmarcar.Click += new System.EventHandler(this.BtnDesmarcar_Click);
            // 
            // BtnIniciarReparacao
            // 
            this.BtnIniciarReparacao.Location = new System.Drawing.Point(33, 17);
            this.BtnIniciarReparacao.Name = "BtnIniciarReparacao";
            this.BtnIniciarReparacao.Size = new System.Drawing.Size(84, 44);
            this.BtnIniciarReparacao.TabIndex = 56;
            this.BtnIniciarReparacao.Text = "Iniciar Reparação";
            this.BtnIniciarReparacao.UseVisualStyleBackColor = true;
            this.BtnIniciarReparacao.Click += new System.EventHandler(this.BtnIniciarReparacao_Click);
            // 
            // BtnRemarcar
            // 
            this.BtnRemarcar.Location = new System.Drawing.Point(33, 67);
            this.BtnRemarcar.Name = "BtnRemarcar";
            this.BtnRemarcar.Size = new System.Drawing.Size(84, 32);
            this.BtnRemarcar.TabIndex = 55;
            this.BtnRemarcar.Text = "Remarcar";
            this.BtnRemarcar.UseVisualStyleBackColor = true;
            this.BtnRemarcar.Click += new System.EventHandler(this.BtnRemarcar_Click);
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.tabPage4);
            this.tabPage9.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabPage9.Location = new System.Drawing.Point(223, 3);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.SelectedIndex = 0;
            this.tabPage9.Size = new System.Drawing.Size(220, 256);
            this.tabPage9.TabIndex = 1;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.NVModeloCmbBox);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Controls.Add(this.NVMarcaCmbBox);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.NVAnoTxtBox);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.PVValidarPicBox);
            this.tabPage4.Controls.Add(this.label5);
            this.tabPage4.Controls.Add(this.NVMatTxtBoxC);
            this.tabPage4.Controls.Add(this.NVMatTxtBoxA);
            this.tabPage4.Controls.Add(this.NVMatTxtBoxB);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(212, 230);
            this.tabPage4.TabIndex = 0;
            this.tabPage4.Text = "Veículo";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // NVModeloCmbBox
            // 
            this.NVModeloCmbBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.NVModeloCmbBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.NVModeloCmbBox.DropDownHeight = 1;
            this.NVModeloCmbBox.DropDownWidth = 21;
            this.NVModeloCmbBox.FormattingEnabled = true;
            this.NVModeloCmbBox.IntegralHeight = false;
            this.NVModeloCmbBox.ItemHeight = 13;
            this.NVModeloCmbBox.Location = new System.Drawing.Point(74, 82);
            this.NVModeloCmbBox.MaxDropDownItems = 1;
            this.NVModeloCmbBox.Name = "NVModeloCmbBox";
            this.NVModeloCmbBox.Size = new System.Drawing.Size(121, 21);
            this.NVModeloCmbBox.TabIndex = 26;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(18, 34);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(52, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "Matrícula";
            // 
            // NVMarcaCmbBox
            // 
            this.NVMarcaCmbBox.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.NVMarcaCmbBox.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.NVMarcaCmbBox.DropDownHeight = 1;
            this.NVMarcaCmbBox.FormattingEnabled = true;
            this.NVMarcaCmbBox.IntegralHeight = false;
            this.NVMarcaCmbBox.Location = new System.Drawing.Point(74, 56);
            this.NVMarcaCmbBox.MaxDropDownItems = 1;
            this.NVMarcaCmbBox.Name = "NVMarcaCmbBox";
            this.NVMarcaCmbBox.Size = new System.Drawing.Size(121, 21);
            this.NVMarcaCmbBox.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(18, 86);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(42, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Modelo";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(142, 34);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "-";
            // 
            // NVAnoTxtBox
            // 
            this.NVAnoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NVAnoTxtBox.Location = new System.Drawing.Point(74, 109);
            this.NVAnoTxtBox.MaxLength = 4;
            this.NVAnoTxtBox.Name = "NVAnoTxtBox";
            this.NVAnoTxtBox.Size = new System.Drawing.Size(65, 20);
            this.NVAnoTxtBox.TabIndex = 6;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(102, 34);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 13);
            this.label9.TabIndex = 23;
            this.label9.Text = "-";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(18, 111);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(26, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Ano";
            // 
            // PVValidarPicBox
            // 
            this.PVValidarPicBox.Location = new System.Drawing.Point(87, 154);
            this.PVValidarPicBox.Name = "PVValidarPicBox";
            this.PVValidarPicBox.Size = new System.Drawing.Size(50, 50);
            this.PVValidarPicBox.TabIndex = 22;
            this.PVValidarPicBox.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Marca";
            // 
            // NVMatTxtBoxC
            // 
            this.NVMatTxtBoxC.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NVMatTxtBoxC.Location = new System.Drawing.Point(154, 31);
            this.NVMatTxtBoxC.MaxLength = 2;
            this.NVMatTxtBoxC.Name = "NVMatTxtBoxC";
            this.NVMatTxtBoxC.Size = new System.Drawing.Size(25, 20);
            this.NVMatTxtBoxC.TabIndex = 3;
            this.NVMatTxtBoxC.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NVMatTxtBoxC.TextChanged += new System.EventHandler(this.NVMatTxtBoxC_TextChanged);
            this.NVMatTxtBoxC.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NVMatTxtBoxC_KeyPress);
            // 
            // NVMatTxtBoxA
            // 
            this.NVMatTxtBoxA.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NVMatTxtBoxA.Location = new System.Drawing.Point(74, 31);
            this.NVMatTxtBoxA.MaxLength = 2;
            this.NVMatTxtBoxA.Name = "NVMatTxtBoxA";
            this.NVMatTxtBoxA.Size = new System.Drawing.Size(25, 20);
            this.NVMatTxtBoxA.TabIndex = 1;
            this.NVMatTxtBoxA.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NVMatTxtBoxA.TextChanged += new System.EventHandler(this.NVMatTxtBoxA_TextChanged);
            // 
            // NVMatTxtBoxB
            // 
            this.NVMatTxtBoxB.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NVMatTxtBoxB.Location = new System.Drawing.Point(114, 31);
            this.NVMatTxtBoxB.MaxLength = 2;
            this.NVMatTxtBoxB.Name = "NVMatTxtBoxB";
            this.NVMatTxtBoxB.Size = new System.Drawing.Size(25, 20);
            this.NVMatTxtBoxB.TabIndex = 2;
            this.NVMatTxtBoxB.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NVMatTxtBoxB.TextChanged += new System.EventHandler(this.NVMatTxtBoxB_TextChanged);
            this.NVMatTxtBoxB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.NVMatTxtBoxB_KeyPress);
            // 
            // tabcontrol
            // 
            this.tabcontrol.Controls.Add(this.tabPage5);
            this.tabcontrol.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabcontrol.Location = new System.Drawing.Point(3, 3);
            this.tabcontrol.Name = "tabcontrol";
            this.tabcontrol.SelectedIndex = 0;
            this.tabcontrol.Size = new System.Drawing.Size(220, 256);
            this.tabcontrol.TabIndex = 0;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.PCValidarPicBox);
            this.tabPage5.Controls.Add(this.NCNIFTxtBox);
            this.tabPage5.Controls.Add(this.label2);
            this.tabPage5.Controls.Add(this.label3);
            this.tabPage5.Controls.Add(this.NCContactoTxtBox);
            this.tabPage5.Controls.Add(this.NCNomeTxtBox);
            this.tabPage5.Controls.Add(this.NCApelidoTxtBox);
            this.tabPage5.Controls.Add(this.label1);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(212, 230);
            this.tabPage5.TabIndex = 0;
            this.tabPage5.Text = "Cliente";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // PCValidarPicBox
            // 
            this.PCValidarPicBox.Location = new System.Drawing.Point(86, 154);
            this.PCValidarPicBox.Name = "PCValidarPicBox";
            this.PCValidarPicBox.Size = new System.Drawing.Size(50, 50);
            this.PCValidarPicBox.TabIndex = 11;
            this.PCValidarPicBox.TabStop = false;
            // 
            // NCNIFTxtBox
            // 
            this.NCNIFTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NCNIFTxtBox.Location = new System.Drawing.Point(76, 31);
            this.NCNIFTxtBox.MaxLength = 9;
            this.NCNIFTxtBox.Name = "NCNIFTxtBox";
            this.NCNIFTxtBox.Size = new System.Drawing.Size(119, 20);
            this.NCNIFTxtBox.TabIndex = 0;
            this.NCNIFTxtBox.TextChanged += new System.EventHandler(this.NCNIFTxtBox_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(20, 112);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Contacto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 86);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "Apelido";
            // 
            // NCContactoTxtBox
            // 
            this.NCContactoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NCContactoTxtBox.Location = new System.Drawing.Point(76, 109);
            this.NCContactoTxtBox.MaxLength = 9;
            this.NCContactoTxtBox.Name = "NCContactoTxtBox";
            this.NCContactoTxtBox.Size = new System.Drawing.Size(119, 20);
            this.NCContactoTxtBox.TabIndex = 0;
            // 
            // NCNomeTxtBox
            // 
            this.NCNomeTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NCNomeTxtBox.Location = new System.Drawing.Point(76, 57);
            this.NCNomeTxtBox.Name = "NCNomeTxtBox";
            this.NCNomeTxtBox.Size = new System.Drawing.Size(119, 20);
            this.NCNomeTxtBox.TabIndex = 0;
            // 
            // NCApelidoTxtBox
            // 
            this.NCApelidoTxtBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NCApelidoTxtBox.Location = new System.Drawing.Point(76, 83);
            this.NCApelidoTxtBox.Name = "NCApelidoTxtBox";
            this.NCApelidoTxtBox.Size = new System.Drawing.Size(119, 20);
            this.NCApelidoTxtBox.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(24, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "NIF";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Nome";
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage3);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tabControl2.Location = new System.Drawing.Point(3, 259);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(596, 142);
            this.tabControl2.TabIndex = 2;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.PMValidarPicBox);
            this.tabPage3.Controls.Add(this.SubTimeBtn);
            this.tabPage3.Controls.Add(this.SubDaysBtn);
            this.tabPage3.Controls.Add(this.AddDaysBtn);
            this.tabPage3.Controls.Add(this.AddTimeBtn);
            this.tabPage3.Controls.Add(this.NMTxtBoxData);
            this.tabPage3.Controls.Add(this.NMTxtBoxHora);
            this.tabPage3.Controls.Add(this.label12);
            this.tabPage3.Controls.Add(this.label11);
            this.tabPage3.Controls.Add(this.BtnCriarMarcacao);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(588, 116);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Data/Hora";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(388, 3);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 21;
            this.label16.Text = "label16";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(296, 3);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(41, 13);
            this.label15.TabIndex = 20;
            this.label15.Text = "label15";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(205, 3);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(41, 13);
            this.label14.TabIndex = 19;
            this.label14.Text = "label14";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(109, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(41, 13);
            this.label13.TabIndex = 18;
            this.label13.Text = "label13";
            // 
            // PMValidarPicBox
            // 
            this.PMValidarPicBox.Location = new System.Drawing.Point(359, 32);
            this.PMValidarPicBox.Name = "PMValidarPicBox";
            this.PMValidarPicBox.Size = new System.Drawing.Size(50, 50);
            this.PMValidarPicBox.TabIndex = 17;
            this.PMValidarPicBox.TabStop = false;
            // 
            // SubTimeBtn
            // 
            this.SubTimeBtn.Location = new System.Drawing.Point(226, 67);
            this.SubTimeBtn.Name = "SubTimeBtn";
            this.SubTimeBtn.Size = new System.Drawing.Size(76, 23);
            this.SubTimeBtn.TabIndex = 16;
            this.SubTimeBtn.Text = "-";
            this.SubTimeBtn.UseVisualStyleBackColor = true;
            this.SubTimeBtn.Click += new System.EventHandler(this.SubTimeBtn_Click);
            // 
            // SubDaysBtn
            // 
            this.SubDaysBtn.Location = new System.Drawing.Point(66, 68);
            this.SubDaysBtn.Name = "SubDaysBtn";
            this.SubDaysBtn.Size = new System.Drawing.Size(76, 23);
            this.SubDaysBtn.TabIndex = 13;
            this.SubDaysBtn.Text = "-";
            this.SubDaysBtn.UseVisualStyleBackColor = true;
            this.SubDaysBtn.Click += new System.EventHandler(this.SubDaysBtn_Click);
            // 
            // AddDaysBtn
            // 
            this.AddDaysBtn.Location = new System.Drawing.Point(66, 25);
            this.AddDaysBtn.Name = "AddDaysBtn";
            this.AddDaysBtn.Size = new System.Drawing.Size(76, 23);
            this.AddDaysBtn.TabIndex = 3;
            this.AddDaysBtn.Text = "+";
            this.AddDaysBtn.UseVisualStyleBackColor = true;
            this.AddDaysBtn.Click += new System.EventHandler(this.AddDaysBtn_Click);
            // 
            // AddTimeBtn
            // 
            this.AddTimeBtn.Location = new System.Drawing.Point(226, 24);
            this.AddTimeBtn.Name = "AddTimeBtn";
            this.AddTimeBtn.Size = new System.Drawing.Size(76, 23);
            this.AddTimeBtn.TabIndex = 14;
            this.AddTimeBtn.Text = "+";
            this.AddTimeBtn.UseVisualStyleBackColor = true;
            this.AddTimeBtn.Click += new System.EventHandler(this.AddTimeBtn_Click);
            // 
            // NMTxtBoxData
            // 
            this.NMTxtBoxData.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NMTxtBoxData.Location = new System.Drawing.Point(61, 48);
            this.NMTxtBoxData.MaxLength = 9;
            this.NMTxtBoxData.Name = "NMTxtBoxData";
            this.NMTxtBoxData.ReadOnly = true;
            this.NMTxtBoxData.Size = new System.Drawing.Size(86, 20);
            this.NMTxtBoxData.TabIndex = 12;
            this.NMTxtBoxData.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.NMTxtBoxData.TextChanged += new System.EventHandler(this.NMTxtBoxData_TextChanged);
            // 
            // NMTxtBoxHora
            // 
            this.NMTxtBoxHora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.NMTxtBoxHora.Location = new System.Drawing.Point(221, 47);
            this.NMTxtBoxHora.MaxLength = 9;
            this.NMTxtBoxHora.Name = "NMTxtBoxHora";
            this.NMTxtBoxHora.ReadOnly = true;
            this.NMTxtBoxHora.Size = new System.Drawing.Size(86, 20);
            this.NMTxtBoxHora.TabIndex = 15;
            this.NMTxtBoxHora.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(22, 52);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "Data";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(182, 51);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(30, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "Hora";
            // 
            // BtnCriarMarcacao
            // 
            this.BtnCriarMarcacao.Location = new System.Drawing.Point(464, 35);
            this.BtnCriarMarcacao.Name = "BtnCriarMarcacao";
            this.BtnCriarMarcacao.Size = new System.Drawing.Size(84, 44);
            this.BtnCriarMarcacao.TabIndex = 2;
            this.BtnCriarMarcacao.Text = "Marcar";
            this.BtnCriarMarcacao.UseVisualStyleBackColor = true;
            this.BtnCriarMarcacao.Click += new System.EventHandler(this.button1_Click);
            // 
            // FormMarcacao
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1000, 430);
            this.Controls.Add(this.DGVMarcacao);
            this.Controls.Add(this.TabConMarcacoes);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormMarcacao";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.DGVMarcacao)).EndInit();
            this.TabConMarcacoes.ResumeLayout(false);
            this.TPNovaMarcacao.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PVValidarPicBox)).EndInit();
            this.tabcontrol.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PCValidarPicBox)).EndInit();
            this.tabControl2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PMValidarPicBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView DGVMarcacao;
        private System.Windows.Forms.TabControl TabConMarcacoes;
        private System.Windows.Forms.TabPage TPNovaMarcacao;
        private System.Windows.Forms.TabControl tabPage9;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TabControl tabcontrol;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox NCApelidoTxtBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox NCNomeTxtBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox NCContactoTxtBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NCNIFTxtBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox NVAnoTxtBox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox NVMatTxtBoxC;
        private System.Windows.Forms.TextBox NVMatTxtBoxB;
        private System.Windows.Forms.TextBox NVMatTxtBoxA;
        private System.Windows.Forms.PictureBox PCValidarPicBox;
        private System.Windows.Forms.Button BtnCriarMarcacao;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox PVValidarPicBox;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox NVMarcaCmbBox;
        private System.Windows.Forms.ComboBox NVModeloCmbBox;
        private System.Windows.Forms.TextBox NMTxtBoxData;
        private System.Windows.Forms.Button SubDaysBtn;
        private System.Windows.Forms.Button AddDaysBtn;
        private System.Windows.Forms.Button SubTimeBtn;
        private System.Windows.Forms.Button AddTimeBtn;
        private System.Windows.Forms.TextBox NMTxtBoxHora;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.PictureBox PMValidarPicBox;
        private System.Windows.Forms.Button BtnDesmarcar;
        private System.Windows.Forms.Button BtnIniciarReparacao;
        private System.Windows.Forms.Button BtnRemarcar;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button BtnCancelar;
        private System.Windows.Forms.Button BtnGuardar;
    }
}

