﻿using OficinaAutomovel.Modelos;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace OficinaAutomovel
{
    public partial class FormPrincipal : Form
    {
        private Cliente Cliente = new Cliente();
        private Veiculo Veiculo = new Veiculo();
        private Marcacao Marcacao = new Marcacao();
        private Reparacao Reparacao = new Reparacao();
        private List<Cliente> ListaDeClientes = new List<Cliente>();
        private List<Veiculo> ListaDeVeiculos = new List<Veiculo>();
        private List<Marcacao> ListaDeMarcacoes = new List<Marcacao>();
        private List<Reparacao> ListaDeReparacoes = new List<Reparacao>();
        private FormMarcacao marcacoes;

        public FormPrincipal()
        {
            InitializeComponent();




        
        }

        private void CarregarBaseDeDados()
        {
            CarregarListaDeClientes();
            CarregarListaDeVeiculos();
            CarregarListaDeMarcacoes();
            CarregarListaDeReparacoes();
            marcacoes = new FormMarcacao(ListaDeClientes, ListaDeVeiculos, ListaDeMarcacoes, ListaDeReparacoes);

        }

        private void FormPrincipal_Load(object sender, EventArgs e)
        {
            CarregarBaseDeDados();

        }

        private void xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void xToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void marcaçõesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            marcacoes.TopLevel = false;
            marcacoes.Dock = DockStyle.Fill;
            PnlPrincipal.Controls.Add(marcacoes);
            marcacoes.Show();
        }

        private void CarregarListaDeMarcacoes()
        {
            ListaDeMarcacoes = null;
            ListaDeMarcacoes = Marcacao.CarregarLista();
        }
        private void CarregarListaDeClientes()
        {
            ListaDeClientes = null;
            ListaDeClientes = Cliente.CarregarLista();
        }
        private void CarregarListaDeVeiculos()
        {
            ListaDeVeiculos = null;
            ListaDeVeiculos = Veiculo.CarregarLista();
        }
        private void CarregarListaDeReparacoes()
        {
            ListaDeReparacoes = null;
            ListaDeReparacoes = Reparacao.CarregarLista();
        }
    }
}
