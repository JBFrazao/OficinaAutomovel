﻿namespace OficinaAutomovel
{
    using OficinaAutomovel.Modelos;
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;

    public partial class FormMarcacao : Form
    {

        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        #region Variáveis

        private DataClasses1DataContext dc = new DataClasses1DataContext();
        private Cliente Cliente = new Cliente();
        private Veiculo Veiculo = new Veiculo();
        private Marcacao Marcacao = new Marcacao();
        private Reparacao Reparacao = new Reparacao();
        private List<Cliente> ListaDeClientes = new List<Cliente>();
        private List<Veiculo> ListaDeVeiculos = new List<Veiculo>();
        private List<Marcacao> ListaDeMarcacoes = new List<Marcacao>();
        private List<Reparacao> ListaDeReparacoes = new List<Reparacao>();
        private DateTime dt = new DateTime();
        private int IDCliente = 0;
        private int IDVeiculo = 0;
        private string matricula = "";

        #endregion

        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        public FormMarcacao(List<Cliente> ListaDeClientes, List<Veiculo> ListaDeVeiculos, List<Marcacao> ListaDeMarcacoes, List<Reparacao> ListaDeReparacoes)
        {
            InitializeComponent();
            
            dt = DateTime.Today;
            dt = dt.AddHours(8);
            NVMarcaCmbBox.DataSource = from Veiculo in dc.Veiculos select Veiculo.Marca;
            NVModeloCmbBox.DataSource = from Veiculo in dc.Veiculos select Veiculo.Modelo;
            DGVMarcacao.Visible = true;
            NVMarcaCmbBox.Text = null;
            NVModeloCmbBox.Text = null;
            UpdateTimeMarcacoes();
            AtualizarDadosDGVM();
            this.ListaDeClientes = ListaDeClientes;
            this.ListaDeVeiculos = ListaDeVeiculos;
            this.ListaDeMarcacoes = ListaDeMarcacoes;
            this.ListaDeReparacoes = ListaDeReparacoes;
            PesquisarMarcacao();

           

        }


        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        #region Botão Gravar
        private void button1_Click(object sender, EventArgs e)
        {

            if (PesquisarRepetidos())
            {
                MessageBox.Show("Já existe uma marcação com esse veículo!", "Inválido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }


            string mensagem = "Foi registado com sucesso o seguinte: ";
            if (PCValidarPicBox.BackColor == Color.Red)
            {
                if (ValidarNovoCliente())
                {
                    IDCliente = Cliente.NovoIdCliente;
                    Cliente.InserirNovoCliente(new Cliente { idCliente = IDCliente, NIF = Convert.ToInt32(NCNIFTxtBox.Text), Nome = NCNomeTxtBox.Text, Apelido = NCApelidoTxtBox.Text, Contacto1 = Convert.ToInt32(NCContactoTxtBox.Text), DataRegisto = DateTime.Today });
                    mensagem += "cliente, ";
                }
                else
                {
                    return;
                }

            }
            


            if (PVValidarPicBox.BackColor == Color.Red)
            {
                if (ValidarNovoVeiculo())
                {
                    IDVeiculo = Veiculo.NovoIdVeiculo;
                    string novaMatricula = NVMatTxtBoxA.Text + "-" + NVMatTxtBoxB.Text + "-" + NVMatTxtBoxC.Text;
                    Veiculo.InserirNovoVeiculo(new Veiculo { idVeiculo = IDVeiculo, Matricula = novaMatricula, Marca = NVMarcaCmbBox.Text, Modelo = NVModeloCmbBox.Text, Ano = Convert.ToInt32(NVAnoTxtBox.Text), DataRegisto = DateTime.Today});
                    mensagem += "veículo, ";
                }
                else
                {
                    return;
                }
                
            }
            if (ValidarNovoCliente() && ValidarNovoVeiculo() && ValidarNovaMarcacao())
            {
                //if true grava a marcação
                Marcacao.InserirNovaMarcacao(new Marcacao { idMarcacao = Marcacao.NovoIdMarcacao, idCliente = IDCliente, idVeiculo = IDVeiculo, Data = dt.Date, Hora = TimeSpan.Parse(NMTxtBoxHora.Text), Estado = "em espera...", DataRegisto = DateTime.Today});
                mensagem += "marcação.";
                MessageBox.Show(mensagem, "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                return;
            }


            CarregarListaDeClientes();
            CarregarListaDeVeiculos();
            CarregarListaDeMarcacoes();
            label13.Text = string.Format("idVeiculo - {0}", Veiculo.NovoIdVeiculo);
            label14.Text = string.Format("idCliente - {0}", Cliente.NovoIdCliente);
            label15.Text = string.Format("idMarcacao - {0}", Marcacao.NovoIdMarcacao);
            label16.Text = string.Format("idReparacao - {0}", Reparacao.NovoIdReparacao);

            LimparCampos();
            PesquisarMarcacao();
            AtualizarDadosDGVM();

        }

        private bool PesquisarRepetidos()
        {
            foreach (var m in ListaDeMarcacoes)
            {
                if (m.idVeiculo == IDVeiculo && m.Estado == "em espera...")
                {
                    return true;
                }
            }
            return false;
        }


        #endregion
        #region Validações de Cliente, Veículo e Marcação
        private bool ValidarNovaMarcacao()
        {
            if (PMValidarPicBox.BackColor == Color.Red)
            {
                MessageBox.Show("Data indisponível!", "Inválido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;

        }
        private bool ValidarNovoCliente()
        {
            if (!Int32.TryParse(NCNIFTxtBox.Text, out int nif) || NCNIFTxtBox.TextLength < 9)
            {
                MessageBox.Show("Falta preencher o campo nif!", "Inválido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else if (NCNomeTxtBox.TextLength == 0)
            {
                MessageBox.Show("Falta preencher o campo nome!", "Inválido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else if (NCApelidoTxtBox.TextLength == 0)
            {
                MessageBox.Show("Falta preencher o campo apelido!", "Inválido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else if (!Int32.TryParse(NCContactoTxtBox.Text, out int contacto) || NCContactoTxtBox.TextLength < 9)
            {
                MessageBox.Show("Falta preencher o campo contacto!", "Inválido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }

        private bool ValidarNovoVeiculo()
        {
            if (NVMatTxtBoxA.TextLength + NVMatTxtBoxB.TextLength + NVMatTxtBoxC.TextLength != 6)
            {
                MessageBox.Show("Falta preencher o campo matrícula!", "Inválido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else if (NVMarcaCmbBox.Text.Length == 0)
            {
                MessageBox.Show("Falta preencher o campo marca!", "Inválido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else if (NVModeloCmbBox.Text.Length == 0)
            {
                MessageBox.Show("Falta preencher o campo modelo!", "Inválido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            else if (!Int32.TryParse(NCContactoTxtBox.Text, out int contacto) || NCContactoTxtBox.TextLength < 4)
            {
                MessageBox.Show("Falta preencher o campo ano!", "Inválido!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }
            return true;
        }
        #endregion
        #region Pesquisar Cliente (NIF)
        private void NCNIFTxtBox_TextChanged(object sender, EventArgs e)
        {
            if (NCNIFTxtBox.TextLength == 9)
            {
                var listaClientes = from Cliente in dc.Clientes select Cliente;

                foreach (var c in listaClientes)
                {
                    if (NCNIFTxtBox.Text == c.NIF.ToString())
                    {
                        PCValidarPicBox.BackColor = Color.Green;        //substituir por imagem
                        NCNomeTxtBox.Text = c.Nome;
                        NCApelidoTxtBox.Text = c.Apelido;
                        NCContactoTxtBox.Text = c.Contacto1.ToString();
                        NCNomeTxtBox.ReadOnly = true;
                        NCApelidoTxtBox.ReadOnly = true;
                        NCContactoTxtBox.ReadOnly = true;
                        IDCliente = c.idCliente;
                        return;
                    }
                    else
                    {
                        PCValidarPicBox.BackColor = Color.Red;    //substituir por imagem
                        IDCliente = Cliente.NovoIdCliente;
                    }
                }
            }
            else if (NCNIFTxtBox.TextLength < 9 && NCNomeTxtBox.ReadOnly == true)
            {
                PCValidarPicBox.BackColor = Color.Transparent;  //substituir por imagem
                NCNomeTxtBox.Text = "";
                NCApelidoTxtBox.Text = "";
                NCContactoTxtBox.Text = "";
                NCNomeTxtBox.ReadOnly = false;
                NCApelidoTxtBox.ReadOnly = false;
                NCContactoTxtBox.ReadOnly = false;

            }

        }
        #endregion
        #region Pesquisar Veículo (Matrícula)
        private void PesquisarMatricula()
        {
            if (NVMatTxtBoxA.TextLength + NVMatTxtBoxB.TextLength + NVMatTxtBoxC.TextLength == 6)
            {
                var listaVeiculos = from Veiculo in dc.Veiculos select Veiculo;

                foreach (var v in listaVeiculos)
                {
                    if (NVMatTxtBoxA.Text + "-" + NVMatTxtBoxB.Text + "-" + NVMatTxtBoxC.Text == v.Matricula)
                    {
                        PVValidarPicBox.BackColor = Color.Green;  //substituir por imagem
                        NVMarcaCmbBox.Text = v.Marca;
                        NVModeloCmbBox.Text = v.Modelo;
                        NVAnoTxtBox.Text = v.Ano.ToString();
                        NVMarcaCmbBox.Enabled = false;
                        NVModeloCmbBox.Enabled = false;
                        NVAnoTxtBox.ReadOnly = true;
                        IDVeiculo = v.idVeiculo;
                        return;
                    }
                    else
                    {
                        PVValidarPicBox.BackColor = Color.Red;  //substituir por imagem
                        IDVeiculo = Veiculo.NovoIdVeiculo;
                    }
                }
            }
            else if ((NVMatTxtBoxA.TextLength + NVMatTxtBoxB.TextLength + NVMatTxtBoxC.TextLength < 6) && NVMarcaCmbBox.Enabled == false)
            {
                PVValidarPicBox.BackColor = Color.Transparent; //substituir por imagem
                NVMarcaCmbBox.Text = "";
                NVModeloCmbBox.Text = "";
                NVAnoTxtBox.Text = "";
                NVMarcaCmbBox.Enabled = true;
                NVModeloCmbBox.Enabled = true;
                NVAnoTxtBox.ReadOnly = false;
                return;
            }

        }

        private void NVMatTxtBoxA_TextChanged(object sender, EventArgs e)
        {
            if (NVMatTxtBoxA.TextLength == 2)
            {
                NVMatTxtBoxB.Select();
                NVMatTxtBoxA.Text = NVMatTxtBoxA.Text.ToUpper();
            }
            PesquisarMatricula();

        }

        private void NVMatTxtBoxB_TextChanged(object sender, EventArgs e)
        {
            if (NVMatTxtBoxB.TextLength == 2)
            {
                NVMatTxtBoxC.Select();
                NVMatTxtBoxB.Text = NVMatTxtBoxB.Text.ToUpper();

            }
            PesquisarMatricula();
        }

        private void NVMatTxtBoxC_TextChanged(object sender, EventArgs e)
        {
            if (NVMatTxtBoxC.TextLength == 2)
            {
                NVMatTxtBoxC.Text = NVMatTxtBoxC.Text.ToUpper();
            }
            PesquisarMatricula();
        }

        private void NVMatTxtBoxB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (NVMatTxtBoxB.TextLength == 0 && e.KeyChar == (char)8)
            {
                NVMatTxtBoxA.Select();
            }
        }

        private void NVMatTxtBoxC_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (NVMatTxtBoxC.TextLength == 0 && e.KeyChar == (char)8)
            {
                NVMatTxtBoxB.Select();
            }
        }
        #endregion
        #region Pesquisar Marcação (Data/Hora)
        private bool PesquisarMarcacao()
        {
            foreach (var Marcacao in ListaDeMarcacoes)
            {
                //&& Marcacao.Hora.Value.ToString() == dt.ToLongTimeString()
                //Marcacao.Data.Value.ToShortDateString().Equals(dt.ToShortDateString())
                if (Marcacao.Data.Value.ToShortDateString().Equals(dt.ToShortDateString()) && Marcacao.Hora.Value.ToString() == dt.ToLongTimeString() && Marcacao.Estado == "em espera...")
                {
                    PMValidarPicBox.BackColor = Color.Red;
                    return false;
                }
                else
                {
                    PMValidarPicBox.BackColor = Color.Green;
                }
            }
            return true;
        }
        #endregion
        #region Controlos Marcação (botões)

        private void AddDaysBtn_Click(object sender, EventArgs e)
        {
            dt = dt.AddDays(1);
            UpdateTimeMarcacoes();
        }

        private void SubDaysBtn_Click(object sender, EventArgs e)
        {
            if (dt > DateTime.Today.AddDays(1))
            {
                dt = dt.AddDays(-1);
            }
            UpdateTimeMarcacoes();
        }

        private void AddTimeBtn_Click(object sender, EventArgs e)
        {
           
            if (dt.Hour < 20)
            {
                dt = dt.AddMinutes(15);
            }
            UpdateTimeMarcacoes();
        }

        private void SubTimeBtn_Click(object sender, EventArgs e)
        {
            if (dt.Hour > 7)
            {
                if (NMTxtBoxHora.Text != "08:00")
                {
                    dt = dt.AddMinutes(-15);

                }
            }
            UpdateTimeMarcacoes();
        }

        private void UpdateTimeMarcacoes()
        {
            NMTxtBoxData.Text = dt.ToShortDateString();
            NMTxtBoxHora.Text = dt.ToShortTimeString();
            PesquisarMarcacao();
        }

        #endregion

        //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        /// <summary>
        /// Atualiza a DataGridView das Marcações
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NMTxtBoxData_TextChanged(object sender, EventArgs e)
        {
            AtualizarDadosDGVM();
        }

        private void AtualizarDadosDGVM()
        {
            DGVMarcacao.DataSource = null;
            DGVMarcacao.DataSource = from Marcacao in dc.Marcacaos
                                     where Marcacao.Data.Value == dt && Marcacao.Estado == "em espera..."
                                     orderby Marcacao.Hora ascending
                                     join Veiculo in dc.Veiculos on Marcacao.idVeiculo equals Veiculo.idVeiculo
                                     join Cliente in dc.Clientes on Marcacao.idCliente equals Cliente.idCliente
                                     select new { Hora = Marcacao.Hora , Matricula = Veiculo.Matricula, Veiculo = Veiculo.Marca + ", " + Veiculo.Modelo, Cliente = Cliente.Nome + " " + Cliente.Apelido };
            DGVMarcacao.Columns[0].Width = 60;
            DGVMarcacao.Columns[1].Width = 60;
            DGVMarcacao.Columns[2].Width = 120;
            DGVMarcacao.Columns[3].Width = 150;
        }


        /// <summary>
        /// Limpar os campos do formulário de cliente & veículo
        /// </summary>
        private void LimparCampos()
        {
            NCNIFTxtBox.Text = null;
            NCNomeTxtBox.Text = null;
            NCApelidoTxtBox.Text = null;
            NCContactoTxtBox.Text = null;
            NVMatTxtBoxA.Text = null;
            NVMatTxtBoxB.Text = null;
            NVMatTxtBoxC.Text = null;
            NVMarcaCmbBox.Text = null;
            NVModeloCmbBox.Text = null;
            NVAnoTxtBox.Text = null;
            PVValidarPicBox.BackColor = Color.Transparent;
            PCValidarPicBox.BackColor = Color.Transparent;

        }



        private void BtnIniciarReparacao_Click(object sender, EventArgs e)
        {
            if(DGVMarcacao.SelectedRows.Count == 1)
            {
                matricula = DGVMarcacao.CurrentRow.Cells[1].Value.ToString();
                int idMarcacao = Marcacao.IniciarReparacao(Marcacao.PesquisarMarcacao(matricula));
                Reparacao.InserirNovaReparacao(new Reparacao { idReparacao = Reparacao.NovoIdReparacao, idMarcacao = idMarcacao, Estado = "pendente" });
                CarregarListaDeMarcacoes();
                CarregarListaDeReparacoes();
                PesquisarMarcacao();
                AtualizarDadosDGVM();
                MessageBox.Show("Reparação iniciada!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }


        #region Carregar Listas
        private void CarregarListaDeMarcacoes()
        {
            ListaDeMarcacoes = null;
            ListaDeMarcacoes = Marcacao.CarregarLista();
        }
        private void CarregarListaDeClientes()
        {
            ListaDeClientes = null;
            ListaDeClientes = Cliente.CarregarLista();
        }
        private void CarregarListaDeVeiculos()
        {
            ListaDeVeiculos = null;
            ListaDeVeiculos = Veiculo.CarregarLista();
        }
        private void CarregarListaDeReparacoes()
        {
            ListaDeReparacoes = null;
            ListaDeReparacoes = Reparacao.CarregarLista();
        }

        #endregion


        private void BtnRemarcar_Click(object sender, EventArgs e)
        {
            if (DGVMarcacao.SelectedRows.Count == 1)
            {
                matricula = DGVMarcacao.CurrentRow.Cells[1].Value.ToString();

                BtnIniciarReparacao.Enabled = false;
                BtnRemarcar.Enabled = false;
                BtnGuardar.Enabled = true;
                BtnCancelar.Enabled = true;
                BtnDesmarcar.Enabled = false;
                BtnCriarMarcacao.Enabled = false;
                DGVMarcacao.Enabled = false;
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            if (ValidarNovaMarcacao())
            {
                Marcacao.Remarcar(Marcacao.PesquisarMarcacao(matricula), dt);
                PesquisarMarcacao();
                AtualizarDadosDGVM();

                BtnIniciarReparacao.Enabled = true;
                BtnRemarcar.Enabled = true;
                BtnGuardar.Enabled = false;
                BtnCancelar.Enabled = false;
                BtnDesmarcar.Enabled = true;
                BtnCriarMarcacao.Enabled = true;
                DGVMarcacao.Enabled = true;

                MessageBox.Show("Marcação alterada com sucesso!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
        }


        private void Cancelar_Click(object sender, EventArgs e)
        {
            BtnIniciarReparacao.Enabled = true;
            BtnRemarcar.Enabled = true;
            BtnGuardar.Enabled = false;
            BtnCancelar.Enabled = false;
            BtnDesmarcar.Enabled = true;
            BtnCriarMarcacao.Enabled = true;
            DGVMarcacao.Enabled = true;
        }


        private void BtnDesmarcar_Click(object sender, EventArgs e)
        {
            if (DGVMarcacao.SelectedRows.Count == 1)
            {
                matricula = DGVMarcacao.CurrentRow.Cells[1].Value.ToString();
                Marcacao.Desmarcar(Marcacao.PesquisarMarcacao(matricula));
                PesquisarMarcacao();
                AtualizarDadosDGVM();
                MessageBox.Show("Marcação foi anulada!", "Sucesso!", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }


    }
}
