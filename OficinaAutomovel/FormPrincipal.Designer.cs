﻿namespace OficinaAutomovel
{
    partial class FormPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PnlPrincipal = new System.Windows.Forms.Panel();
            this.MSMenuPrincipal = new System.Windows.Forms.MenuStrip();
            this.marcaçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.xToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.xToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reparaçõesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.faturaçãoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.mensagensToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.MSMenuPrincipal.SuspendLayout();
            this.SuspendLayout();
            // 
            // PnlPrincipal
            // 
            this.PnlPrincipal.Dock = System.Windows.Forms.DockStyle.Right;
            this.PnlPrincipal.Location = new System.Drawing.Point(85, 0);
            this.PnlPrincipal.Name = "PnlPrincipal";
            this.PnlPrincipal.Size = new System.Drawing.Size(1000, 430);
            this.PnlPrincipal.TabIndex = 0;
            // 
            // MSMenuPrincipal
            // 
            this.MSMenuPrincipal.BackColor = System.Drawing.Color.Orange;
            this.MSMenuPrincipal.Dock = System.Windows.Forms.DockStyle.Left;
            this.MSMenuPrincipal.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.MSMenuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.marcaçõesToolStripMenuItem,
            this.xToolStripMenuItem1,
            this.xToolStripMenuItem,
            this.reparaçõesToolStripMenuItem,
            this.faturaçãoToolStripMenuItem,
            this.mensagensToolStripMenuItem,
            this.consultaToolStripMenuItem});
            this.MSMenuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.MSMenuPrincipal.Name = "MSMenuPrincipal";
            this.MSMenuPrincipal.Padding = new System.Windows.Forms.Padding(0);
            this.MSMenuPrincipal.Size = new System.Drawing.Size(85, 430);
            this.MSMenuPrincipal.TabIndex = 1;
            this.MSMenuPrincipal.Text = "menuStrip1";
            // 
            // marcaçõesToolStripMenuItem
            // 
            this.marcaçõesToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.marcaçõesToolStripMenuItem.Name = "marcaçõesToolStripMenuItem";
            this.marcaçõesToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.marcaçõesToolStripMenuItem.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.marcaçõesToolStripMenuItem.RightToLeftAutoMirrorImage = true;
            this.marcaçõesToolStripMenuItem.Size = new System.Drawing.Size(84, 42);
            this.marcaçõesToolStripMenuItem.Text = "Marcações";
            this.marcaçõesToolStripMenuItem.Click += new System.EventHandler(this.marcaçõesToolStripMenuItem_Click);
            // 
            // xToolStripMenuItem1
            // 
            this.xToolStripMenuItem1.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.xToolStripMenuItem1.ForeColor = System.Drawing.Color.White;
            this.xToolStripMenuItem1.Name = "xToolStripMenuItem1";
            this.xToolStripMenuItem1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.xToolStripMenuItem1.Size = new System.Drawing.Size(84, 42);
            this.xToolStripMenuItem1.Text = "Sair";
            this.xToolStripMenuItem1.Click += new System.EventHandler(this.xToolStripMenuItem1_Click);
            // 
            // xToolStripMenuItem
            // 
            this.xToolStripMenuItem.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.xToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.xToolStripMenuItem.Name = "xToolStripMenuItem";
            this.xToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.xToolStripMenuItem.Size = new System.Drawing.Size(84, 32);
            this.xToolStripMenuItem.Text = "_";
            this.xToolStripMenuItem.Click += new System.EventHandler(this.xToolStripMenuItem_Click);
            // 
            // reparaçõesToolStripMenuItem
            // 
            this.reparaçõesToolStripMenuItem.Name = "reparaçõesToolStripMenuItem";
            this.reparaçõesToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.reparaçõesToolStripMenuItem.Size = new System.Drawing.Size(84, 42);
            this.reparaçõesToolStripMenuItem.Text = "Reparações";
            // 
            // faturaçãoToolStripMenuItem
            // 
            this.faturaçãoToolStripMenuItem.Name = "faturaçãoToolStripMenuItem";
            this.faturaçãoToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.faturaçãoToolStripMenuItem.Size = new System.Drawing.Size(84, 42);
            this.faturaçãoToolStripMenuItem.Text = "Faturação";
            // 
            // mensagensToolStripMenuItem
            // 
            this.mensagensToolStripMenuItem.Name = "mensagensToolStripMenuItem";
            this.mensagensToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.mensagensToolStripMenuItem.Size = new System.Drawing.Size(84, 42);
            this.mensagensToolStripMenuItem.Text = "Mensagens";
            // 
            // consultaToolStripMenuItem
            // 
            this.consultaToolStripMenuItem.Name = "consultaToolStripMenuItem";
            this.consultaToolStripMenuItem.Padding = new System.Windows.Forms.Padding(0, 10, 0, 10);
            this.consultaToolStripMenuItem.Size = new System.Drawing.Size(84, 42);
            this.consultaToolStripMenuItem.Text = "Consulta";
            // 
            // FormPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1085, 430);
            this.Controls.Add(this.PnlPrincipal);
            this.Controls.Add(this.MSMenuPrincipal);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.MSMenuPrincipal;
            this.Name = "FormPrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormPrincipal";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.FormPrincipal_Load);
            this.MSMenuPrincipal.ResumeLayout(false);
            this.MSMenuPrincipal.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel PnlPrincipal;
        private System.Windows.Forms.MenuStrip MSMenuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem marcaçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem xToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reparaçõesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem faturaçãoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem mensagensToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultaToolStripMenuItem;
    }
}